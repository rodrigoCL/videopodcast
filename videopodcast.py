#!/usr/bin/env python
# -*- coding: utf-8 -*-

from flask import Flask, render_template
from flask.ext.compress import Compress
from werkzeug.routing import BaseConverter
from blueprints import home_blueprint, common_blueprint, rest_blueprint
from common import config
from common.utils import cache
import common.log as log


videopodcast = Flask(__name__)
videopodcast.secret_key = config.SECRET_KEY

class RegexConverter(BaseConverter):
    def __init__(self, url_map, *items):
        super(RegexConverter, self).__init__(url_map)
        self.regex = items[0]

# Use the RegexConverter function as a converter
# method for mapped urls
videopodcast.url_map.converters['regex'] = RegexConverter

videopodcast.register_blueprint(common_blueprint.page)
videopodcast.register_blueprint(home_blueprint.page)
videopodcast.register_blueprint(rest_blueprint.page)

Compress(videopodcast)
cache.init_app(videopodcast)

@videopodcast.errorhandler(404)
def page_not_found(e):
    g.config = config
    return render_template('404.html'), 404

@videopodcast.errorhandler(500)
def page_not_found(e):
    g.config = config
    return render_template('500.html'), 500


def gevent():
    from gevent.wsgi import WSGIServer

    videopodcast.logger.info("Running Gevent on port %s", config.port)

    http_server = WSGIServer(('', config.port), videopodcast)
    http_server.serve_forever()


def tornado():
    from tornado.wsgi import WSGIContainer
    from tornado.httpserver import HTTPServer
    from tornado.ioloop import IOLoop

    videopodcast.logger.info("Running Tornado on port %s", config.port)

    http_server = HTTPServer(WSGIContainer(videopodcast))
    http_server.listen(config.port)
    IOLoop.instance().start()


def builtin():
    videopodcast.logger.info("Running Built-in development server on port %s", config.port)
    videopodcast.run(host="0.0.0.0", port=config.port, debug=config.debug)


def start_server():
    videopodcast.logger.info("Parsing options ...")
    #Command Arguments parser and help generator
    from optparse import OptionParser

    _cmd_parser = OptionParser(usage="usage: %prog [options]", version="CNN Video Podcast Feeder {0}".format("0.1"))
    _opt = _cmd_parser.add_option
    _opt("--debug", action="store_true", help="Debug Mode.")
    _opt("--tornado", action="store_true", help="Tornado non-blocking web server.")
    _opt("--gevent", action="store_true", help="Gevent non-blocking web server.")
    _opt("--gunicorn", action="store_true", help="Gevent non-blocking web server.")
    _opt("--builtin", action="store_true", help="Built-in Flask web development server.")
    _opt("--port", help="Port to run")
    _opt("--daemon", action="store_true", help="Daemonize")
    _opt("--start", action="store_true", help="Daemonize")
    _opt("--stop", action="store_true", help="Daemonize")
    _opt("--restart", action="store_true", help="Daemonize")

    _cmd_options, _cmd_args = _cmd_parser.parse_args()

    opt, args, parser = _cmd_options, _cmd_args, _cmd_parser

    if opt.debug:
        config.debug = opt.debug

    if config.debug:
        videopodcast.logger.info("DEBUG MODE ON!")

    log.set_logging(videopodcast)


    videopodcast.logger.info("        (              )\ )       (                      )")
    videopodcast.logger.info("   )   (    )\ )   (      (()/(       )\ )         )      ( /(")
    videopodcast.logger.info("  /((  )\  (()/(  ))\  (   /(_)) (   (()/(  (   ( /(  (   )\())")
    videopodcast.logger.info(" (_))\((_)  ((_))/((_) )\ (_))   )\   ((_)) )\  )(_)) )\ (_))/")
    videopodcast.logger.info(" _)((_)(_)  _| |(_))  ((_)| _ \ ((_)  _| | ((_)((_)_ ((_)| |_")
    videopodcast.logger.info(" \ V / | |/ _` |/ -_)/ _ \|  _// _ \/ _` |/ _| / _` |(_-<|  _|")
    videopodcast.logger.info("  \_/  |_|\__,_|\___|\___/|_|  \___/\__,_|\__| \__,_|/__/ \__|")


    if opt.port:
        config.port = opt.port

    videopodcast.logger.info("Starting on port %s", config.port)

    if opt.daemon:
        from daemon import Daemon

        class ServerDaemon(Daemon):
            def run(self):
                if opt.tornado:
                    videopodcast.logger.info("WEB SERVER: Using Tornado")
                    tornado()
                elif opt.gevent:
                    videopodcast.logger.info("WEB SERVER: Using Gevent")
                    gevent()
                else:
                    videopodcast.logger.info("WEB SERVER: Using builtin")
                    builtin()

        daemon = ServerDaemon('/tmp/server-daemon.pid')

        if opt.start:
            daemon.start()
        elif opt.stop:
            daemon.stop()
        elif opt.restart:
            daemon.restart()

    elif opt.tornado:
        videopodcast.logger.info("WEB SERVER: Using Tornado")
        tornado()
    elif opt.gunicorn:
        videopodcast.logger.info("WEB SERVER: Using gunicorn")
        #gunicorn()
    elif opt.gevent:
        videopodcast.logger.info("WEB SERVER: Using Gevent")
        gevent()
    else:
        videopodcast.logger.info("WEB SERVER: Using builtin")
        builtin()


if __name__ == "__main__":
    start_server()

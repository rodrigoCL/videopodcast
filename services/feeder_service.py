#!/usr/bin/env python
# -*- coding: utf-8 -*-
import feedparser
from common.log import logger
from parsers.cnn_parser import CnnParser
from repositories.feeder_repository import FeederRepository
from repositories.user_repository import UserRepository


class FeederService(object):
    def feed_all(self):

        for user in UserRepository().get_all():
            logger().info("Getting Feed for user {0}".format(user.id))
            self.feed(user.id)

    def get_rss_urls(self, user_id):
        feeds = FeederRepository().get_rss_urls(user_id)

        if feeds is None:
            return []
        else:
            return feeds

    def save_rss(self, url_id, feed):
        FeederRepository().save_rss(url_id, feed)

    def save_episode(self, url_id, episode):
         FeederRepository().save_episode(url_id, episode)

    def save_feed(self, user_id, url_id, title, summary, src):
        pass

    def feed(self, user):
        for url in self.get_rss_urls(user.id):
            d = feedparser.parse(url.rss_url)

            logger().info("Feeding atom {0}".format(url.rss_url))

            self.save_rss(url.id, CnnParser().parse(d))

            for entry in  CnnParser().parse_entries(d):
                self.save_episode(url.id, entry)

    def get_feed(self, user):
        return FeederRepository().get_feed(user)

    def add_feed(self, user, url):
        FeederRepository().add_feed(user, url)

    def get_episodies(self, feed):
        return FeederRepository().get_episodies(feed)

    def get_feeds(self, user):
        return FeederRepository().get_feeds(user)




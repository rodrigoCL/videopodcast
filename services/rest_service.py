#!/usr/bin/env python
# -*- coding: utf-8 -*-
from flask import jsonify, session
from services.feeder_service import FeederService
from services.user_service import UserService


class RestService(object):

    def feed(self):
        feeder_service = FeederService()
        user = UserService().get_from_guid(session['guid'])
        episodies = []
        title = "To start Feeding you need insert a url feed from CNN. To start typing a feed press E"
        summary = "This application can be used only with the keyboard. To togle shortcuts keyboard in any moment press Ctrl+x. To get more help press H"
        poster = ""

        if not user is None:
            feeder_service.feed(user)

            feed = feeder_service.get_feed(user)

            if not feed is None:
                title = feed.title
                summary = feed.summary
                poster = feed.poster
                episodies = feeder_service.get_episodies(feed)

        if len(episodies) > 0:
            first = episodies[0]['src']
            first_title = episodies[0]['title']
        else:
            first = ""
            first_title = ""

        return jsonify({
            "title": title
            , "subtitle": summary
            , "poster": poster
            , "episodies": episodies
            , "first": first
            , "first_title": first_title
            , "loading": "<div id=\"my-player\">Loading the player...</div>"
        })

    def feeds(self):
        user = UserService().get_from_guid(session['guid'])

        if not user is None:
            feeds = FeederService().get_feeds(user)

            if feeds is None:
                feeds = []
        else:
            feeds = []


        return jsonify({
            'episodies': [{'id': e.id, 'text': e.rss_url} for e in feeds]
        })

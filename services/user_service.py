#!/usr/bin/env python
# -*- coding: utf-8 -*-
from repositories.user_repository import UserRepository


class UserService(object):

    def get_all(self):
        user_repository = UserRepository()

        return user_repository.get_all()

    def save_guid(self, guid):
        user_repository = UserRepository()

        return user_repository.save_guid(guid)

    def get_from_guid(self, guid):
        user_repository = UserRepository()

        return user_repository.get_from_guid(guid)
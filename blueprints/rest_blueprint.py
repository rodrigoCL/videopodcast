#!/usr/bin/env python
# -*- coding: utf-8 -*-
import feedparser
from flask import Blueprint, request, session, jsonify
from services.feeder_service import FeederService
from services.rest_service import RestService
from services.user_service import UserService

page = Blueprint('rest_page', __name__, template_folder='templates/admin')

@page.route("/rest/feed")
def feed():
    if "url" in request.args:
        try:
            feedparser.parse(request.args['url']).feed['title']
        except:
            return jsonify({
                "title": "Invalid RSS URL!"
                , "subtitle": ""
                , "poster": ""
                , "episodies": []
                , "first": ""
                , "first_title": ""
                , "loading": "<div id=\"my-player\">Loading the player...</div>"
            })

        FeederService().add_feed(UserService().get_from_guid(session['guid']), request.args['url'])

    return RestService().feed()

@page.route("/rest/feeds")
def feeds():
    return RestService().feeds()




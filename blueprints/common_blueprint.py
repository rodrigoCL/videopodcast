#!/usr/bin/env python
# -*- coding: utf-8 -*-
from flask import Blueprint

page = Blueprint('common_page', __name__, template_folder='templates/admin')
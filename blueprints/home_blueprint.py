#!/usr/bin/env python
# -*- coding: utf-8 -*-
import uuid
import datetime
from flask import Blueprint, render_template, request, session, make_response
from common.config import COOKIE_DOMAIN
from services.user_service import UserService

page = Blueprint('home_page', __name__, template_folder='templates/home')

@page.route("/")
def index():
    if "videopodcast_guid" in request.cookies:
        session["guid"] = request.cookies.get('videopodcast_guid');

        resp = render_template("home/index.html")
    else:
        session["guid"] = str(uuid.uuid4())

        resp = make_response(render_template("home/index.html"));

        resp.set_cookie(
            'videopodcast_guid'
            , value=session["guid"]
            , expires=(datetime.datetime.now() + datetime.timedelta(365))
        )

    UserService().save_guid(session["guid"])

    return resp

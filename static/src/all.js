/** @jsx React.DOM */
//Creating Space Names
var videopodcast = videopodcast || {};

(function (o) {
    o.utils = o.utils || {};
    o.params = o.params || {};
    o.UI = o.UI || {};
    o.common = o.common || {};
    o.i18n = o.i18n || {};
    o.security = o.security || {};
    o.shares = o.shares || {};
})(videopodcast);

(function (o) {
    o.app = function(){
        var viewer = videopodcast.UI.widgets.viewer;

        videopodcast.UI.addInstance("viewer", React.renderComponent(
          <viewer url="/rest/feed"/>,
          document.getElementById('viewer')
        ));

        $('#feed-button').on('click', function(){
            $('#feed-url').blur();
            videopodcast.UI.getInstance('viewer').load($("#feed-url").val());
        });

        $('#update-button').on('click', function(){
            videopodcast.UI.getInstance('viewer').load();
        });

        videopodcast.UI.init();
    }
})(videopodcast.common);

(function (o) {
    o.widgets = o.widgets || {};
    var _instances = {};

    o.addInstance = function(key, instance){
        _instances[key] = instance;
    }

    o.getInstance = function(key){
        return _instances[key];
    }

    var _selectedepisode = "episode-0";

    o.handleClick = function(id, src, poster, title){
        var selected = $('#'+_selectedepisode);
        var elem = $('#'+id);

        selected.removeClass("player-media-active");
        elem.addClass("player-media-active");

        _selectedepisode = id;

        $("#selected-title").text(title);

        jwplayer("my-player").setup({
           file: src,
           image: poster,
           height: 360,
           width: 640
        });
    }

    var _key_press_delay = 500;
    var _delay = function(button){
        $("#"+button+"-button").addClass("player-nav-button-active");
        setTimeout(function(){$("#"+button+"-button").removeClass("player-nav-button-active");},_key_press_delay);
    }

    var _scrollTo = function(id){
        var container = $('#player-media-content-list'), scrollTo = $(id);

        container.animate({
            scrollTop: scrollTo.offset().top - container.offset().top + container.scrollTop()
        });
    }


    o.left = function(button){
        if(button) {
            _delay("down");
        }
        var idx = parseInt(_selectedepisode.replace("episode-",""));

        if(idx > 0){
            idx--;
            _scrollTo("#episode-"+idx);
            $("#episode-"+idx).click();
        }
    }

    o.right = function(button){
        if(button) {
            _delay("up");
        }
        var idx = parseInt(_selectedepisode.replace("episode-",""));
        idx++;
        if($("#episode-"+idx).length > 0){
            _scrollTo("#episode-"+idx);
            $("#episode-"+idx).click();
        }
    }

    o.up = function(){
        var idx = parseInt(_selectedepisode.replace("episode-",""));
        idx = idx - 2;

        if($("#episode-"+idx).length > 0){
            _scrollTo("#episode-"+idx);
            $("#episode-"+idx).click();
        }
    }

    o.down = function(){
        var idx = parseInt(_selectedepisode.replace("episode-",""));
        idx = idx + 2;

        if($("#episode-"+idx).length > 0){
            _scrollTo("#episode-"+idx);
            $("#episode-"+idx).click();
        }
    }

    o.play = function(){
        _delay("play");
        jwplayer("my-player").play(true);
    }

    o.pause = function(){
        _delay("pause");
        jwplayer("my-player").pause(true);
    }

    o.stop = function(){
        _delay("stop");
        jwplayer("my-player").stop(true);
    }

    o.forward = function(){
        _delay("forward");
        var player = jwplayer("my-player");

        player.seek(player.getPosition()+10);
    }

    o.backward = function(){
        _delay("backward");
        var player = jwplayer("my-player");

        player.seek(player.getPosition()-10);
    }

    o.openHelp = function(){
        $("#help-modal").modal('show');
    }

    o.init = function(){
        Mousetrap.bind('left', function(){
               o.left();
        });

        Mousetrap.bind('right', function(){
               o.right();
        });

        Mousetrap.bind('up', function(){
               o.up();
        });

        Mousetrap.bind('down', function(){
               o.down();
        });

        Mousetrap.bind('h', function(){
               videopodcast.UI.openHelp();
        });

        Mousetrap.bind('u', function(){
                $("#up-button").click();
        });

        Mousetrap.bind('d', function(){
               $("#down-button").click();
        });

        Mousetrap.bind('p', function(){
                $("#play-button").click();
        });

        Mousetrap.bind('s', function(){
               $("#stop-button").click();
        });

        Mousetrap.bind('space', function(){
               $("#pause-button").click();
        });

        Mousetrap.bind('b', function(){
               $("#backward-button").click();
        });

        Mousetrap.bind('f', function(){
               $("#forward-button").click();
        });

        Mousetrap.bind('e', function(){
               $("#feed-url").focus();
        });

        Mousetrap.bind('return', function(){
               $("#feed-button").click();
        });
    }
})(videopodcast.UI);

(function (o) {
    var media = React.createClass({
         render: function() {
            var my_class = "player-media " + this.props.active + " col-lg-6";
            var id = this.props.id;
            var src = this.props.episode.src;
            var poster = this.props.data.poster;
            var title = this.props.episode.title;
            var click = function(){
                videopodcast.UI.handleClick(id, src, poster, title);
            }

            return (
             <div onClick={click} id={this.props.id} className={my_class}>
                <div>
                    <h6>{this.props.episode.title}</h6>
                    <div><span dangerouslySetInnerHTML={{__html: this.props.episode.summary}} /></div>
                </div>
             </div>
         );
      }
    });

    var media_list = React.createClass({
         render: function() {
            var rows = [];
            var active = "player-media-active";
            var i = 0;
            var data = this.props.data;
            this.props.data.episodies.forEach(function(episode) {
                var id = "episode-"+i;
                rows.push(<media data={data} episode={episode} active={active} id={id}/>);
                active="";
                i++;
            });

            return (
             <div className="row">
                {rows}
             </div>
         );
      }
    });

    var media_player = React.createClass({
         render: function() {
            return (
                <div>
                <h4 id="selected-title">{this.props.data.first_title}</h4>
                <div><span dangerouslySetInnerHTML={{__html: this.props.data.loading}} /></div>
                </div>
         );
      }
    });

    var media_content = React.createClass({
         render: function() {
            return (
             <div className="media-content row">
                <div id = "player-media-content"  className="player-media-content col-lg-6"><media_player data={this.props.data}/></div>
                <div id = "player-media-content-list" className="player-media-content-list col-lg-6"><media_list data={this.props.data}/></div>
             </div>
         );
      }
    });

    o.viewer = React.createClass({
      load: function(url) {

        this.setState({data: {"title": "Feeding ...", "episodies": [], "loading": ""}});

        $.ajax({
          url: this.props.url + ((url)? "?url=" + url : ""),
          dataType: 'json',
          success: function(data) {
            this.setState({data: data});

            if($("#my-player").length > 0) {
                jwplayer("my-player").setup({
                    file: this.state.data.first,
                    image: this.state.data.poster,
                    width: 640,
                    height: 360
                });
            }
          }.bind(this),
          error: function(xhr, status, err) {
            console.error(this.props.url, status, err.toString());
          }.bind(this)
        });
      },
      componentWillMount: function() {
        this.load();
      },
      getInitialState: function() {
        return {data: {
            "title": "Please add a RSS url to Feed"
            , "episodies": []
            , "loading": ""
        }};
      },
      componentDidMount: function() {

      },
      render: function() {
        return (
         <div>
             <h3><span id="update-button" className="glyphicon glyphicon-repeat"></span> {this.state.data.title}</h3>
             <h4>{this.state.data.subtitle}</h4>
             <media_content data={this.state.data}/>
         </div>
        );
      }
    });
})(videopodcast.UI.widgets);

$(document).ready(function () {
    videopodcast.common.app();
});

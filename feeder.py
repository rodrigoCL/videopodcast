#!/usr/bin/env python
# -*- coding: utf-8 -*-
from common.log import logger
from services.feeder_service import FeederService

if __name__ == "__main__":
    from optparse import OptionParser

    _version = "1.0"
    _cmd_parser = OptionParser(usage="usage: %prog [options]", version="CNN video podcast Feeder {0}".format(_version))
    _opt = _cmd_parser.add_option

    _cmd_options, _cmd_args = _cmd_parser.parse_args()

    opt, args, parser = _cmd_options, _cmd_args, _cmd_parser

    logger().info("Starting Video Podcast Feeding")

    FeederService().feed_all()

    logger().info("Finishing Video Podcast Feeding")


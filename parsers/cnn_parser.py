from common.log import logger

__author__ = 'rodrigo'

class CnnParser(object):
    def parse(self, data):
        title = data["feed"]["title"]
        summary = data["feed"]["summary"]
        poster = data["feed"]["image"]["href"]

        return {
            "title": title
            , "summary": summary
            , "poster": poster
        }

    def parse_entries(self, data):

        entries = []
        for entry in data['entries']:
            entries.append({
                "title": entry["title"]
                , "summary": entry["summary"]
                , "src": entry["link"]
            })

            logger().info("Podcast source found: {0}".format(entry["link"]))

        return entries



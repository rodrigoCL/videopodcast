#!/usr/bin/env python
# -*- coding: utf-8 -*-
from models.user_model import UserModel


class UserRepository(object):
    def get_all(self):
        return UserModel.fetch_all("SELECT * FROM #table#")

    def save_guid(self, guid):
        user = UserModel().fetch_one("WHERE guid = %s", (guid,))

        if user is None:
            UserModel().execute("INSERT INTO #table# (guid) VALUES (%s)", (guid,))

    def get_from_guid(self, guid):
        return UserModel().fetch_one("WHERE guid = %s", (guid,))
#!/usr/bin/env python
# -*- coding: utf-8 -*-
from models.feed_episode import FeedEpisodeModel
from models.user_feed_model import UserFeedModel
from models.user_model import UserModel


class FeederRepository(object):

    def get_rss_urls(self, user_id):
        return UserFeedModel().fetch_all("SELECT * FROM #table# WHERE user_id=%s", (user_id,))

    def save_rss(self, url_id, feed):
        UserFeedModel().execute("""
            UPDATE #table#
            SET title = %s
            , summary = %s
            , poster = %s
            WHERE id = %s
        """, (feed["title"], feed["summary"], feed["poster"], url_id))

    def save_episode(self, url_id, episode):
        FeedEpisodeModel().execute("""
            INSERT INTO #table# (hash, src, title, summary, user_feed_id)
            VALUES (SHA1(CONCAT(%s,'-',%s)), %s, %s, %s, %s)
            ON DUPLICATE KEY UPDATE
            title = %s
            , summary = %s
        """, (
            episode["src"]
            , url_id
            , episode["src"]
            , episode["title"]
            , episode["summary"]
            , url_id
            , episode["title"]
            , episode["summary"]
        ))

    def get_feed(self, user):
        return UserFeedModel().fetch_one("WHERE user_id = %s AND id = %s", (user.id, user.active_feed))

    def add_feed(self, user, url):
        feed = UserFeedModel().fetch_one("WHERE user_id = %s AND rss_url = %s", (user.id, url))

        if feed is None:
            UserFeedModel().execute("""
                INSERT INTO #table# (user_id, rss_url) VALUES (%s, %s)
            """, (user.id, url))

            feed = UserFeedModel().fetch_one("WHERE user_id = %s AND rss_url = %s", (user.id, url))

        UserModel().execute("UPDATE #table# SET active_feed = %s WHERE id = %s", (feed.id, user.id))

    def get_episodies(self, feed):

        return FeedEpisodeModel().fetch_all("WHERE user_feed_id = %s", (feed.id,), as_dict=True)

    def get_feeds(self, user):
        return UserFeedModel().fetch_all("WHERE user_id = %s", (user.id,))




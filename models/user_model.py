#!/usr/bin/env python
# -*- coding: utf-8 -*-

from microorm import Model


class UserModel(Model):
    id = 0
    email = None
    guid = None
    active_feed = None

    @classmethod
    def get_table_name(cls):
        return "users"

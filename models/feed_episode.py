#!/usr/bin/env python
# -*- coding: utf-8 -*-

from microorm import Model


class FeedEpisodeModel(Model):
    src = None
    title = None
    summary = None
    user_feed_id = None
    poster = None

    @classmethod
    def get_table_name(cls):
        return "feed_episodies"

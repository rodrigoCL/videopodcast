#!/usr/bin/env python
# -*- coding: utf-8 -*-

from microorm import Model


class UserFeedModel(Model):
    id = 0
    user_id = None
    rss_url = None
    title = None
    summary = None
    poster = None


    @classmethod
    def get_table_name(cls):
        return "user_feeds"

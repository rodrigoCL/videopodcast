#!/usr/bin/env python
# -*- coding: utf-8 -*-
import inspect
import traceback
from flask import session, request
import mysql.connector

from common import config as Config
import pusher
from common.config import DEFAULT_DATABASE


def send_to_debug_console(sql, type, caller):

    try:
        if "get_modules" in caller:
            return

        if "SELECT * FROM users WHERE" in sql:
            return

        if "get_user_unique_token" in caller:
            return

        if "channel" in session:
            channel = session['channel']

        if not "console_mode" in session and "debug_channel" in request.args:
            channel = request.args['debug_channel']

            if channel == 'off':
                session.pop('channel')
                return

            session['channel'] = channel

        p = pusher.Pusher(
          app_id='63148',
          key='dbd5ff36d9ff3f3ab101',
          secret='61402286fa459a801a2f'
        )

        p[channel].trigger('sql_query_executed', {'message': sql, 'type': type, 'caller': caller})

    except:
        pass


class Database(object):
    def __init__(self, name, type):

        if (name == "auth"):
            connection_data = Config.database[name]
        else:
            try:
                connection_data = Config.database[name][type]
            except:
                connection_data = Config.database[DEFAULT_DATABASE][type]

        self.cnx = mysql.connector.connect(
            user=connection_data["user"]
            , password=connection_data["password"]
            , host=connection_data["host"]
            , database=connection_data["database"]
            , use_unicode=True, charset="utf8"
            , collation="utf8_swedish_ci"
        )

    def cursor(self):
        return self.cnx.cursor()

    def commit(self):
        self.cnx.commit()

    def close(self):
        self.cnx.close()


class Model(object):
    use_auth = False

    def to_dict(self):
        if self is None:
            return None

        return dict((name[0], name[1]) for name in inspect.getmembers(self, lambda a: not (inspect.isroutine(a))) if
                    not name[0].startswith('__'))

    @classmethod
    def fill_tables(cls, sql, tables):
        if len(tables) > 0:
            table_number = 0
            for table in tables:
                sql = sql.replace("#table{0}#".format(table_number), table)

        else:
            sql = sql.replace("#table#", cls.get_table_name())

        return sql

    @classmethod
    def execute(cls, sql, params=(), tables=[], database=None, dbtype='master', as_dict=False):

        if database is None:
            database = DEFAULT_DATABASE

        if cls.use_auth():
            database = "auth"

        try:

            sql = cls.fill_tables(sql, tables)

            cnx = Database(database, dbtype)

            cursor = cnx.cursor()

            cursor.execute(sql, params)

            curframe = inspect.currentframe()
            calframe = inspect.getouterframes(curframe, 2)
            send_to_debug_console(cursor._executed, "success", calframe[1][3])

            inserted_id = cursor.lastrowid
        except:
            print traceback.format_exc()
            curframe = inspect.currentframe()
            calframe = inspect.getouterframes(curframe, 2)
            send_to_debug_console(sql, "error", calframe[1][3])
        finally:
            try:
                cnx.commit()
                cursor.close()
                cnx.close()
                return inserted_id
            except:
                pass

    @classmethod
    def fetch_all(cls, sql=None, params=(), tables=[], database=None, dbtype='master', as_dict=False, multi=False, clean=None):
        attributes = inspect.getmembers(cls, lambda a: not (inspect.isroutine(a)))
        attributes = [a for a in attributes if not (a[0].startswith('__') and a[0].endswith('__'))]

        if sql is None:
            sql = "SELECT "

            separator = ""
            for attribute in attributes:
                sql = "{0} {1}{2}".format(sql, separator, attribute[0])
                separator = ","

            sql = "{0} FROM #table#".format(sql)

        if sql.startswith("WHERE") or sql.startswith("where"):
            where = sql
            sql = "SELECT "

            separator = ""
            for attribute in attributes:
                sql = "{0} {1}{2}".format(sql, separator, attribute[0])
                separator = ","

            sql = "{0} FROM #table# {1}".format(sql, where)

        if database is None:
            database = DEFAULT_DATABASE

        if cls.use_auth():
            database = "auth"

        try:

            sql = cls.fill_tables(sql, tables)

            cnx = Database(database, dbtype)

            cursor = cnx.cursor()
            if not multi:
                cursor.execute(sql, params)
            else:
                statements = sql.split(';')[:-1]
                c = len(statements)
                for statement_index in range(0,c-1):
                    statement = statements[statement_index]
                    cursor.execute(statement)
                    cursor.close()
                    cursor = cnx.cursor()

                cursor.execute(statements[c-1])


            curframe = inspect.currentframe()
            calframe = inspect.getouterframes(curframe, 2)
            send_to_debug_console(cursor._executed, "success", calframe[1][3])

            result = []

            columns = tuple([d[0].decode('utf8') for d in cursor.description])
            for row in cursor:
                row = dict(zip(columns, row))

                if as_dict:
                    result.append(row)
                else:
                    data = cls()

                    for attribute in attributes:
                        if row.has_key(attribute[0]):
                            setattr(data, attribute[0], row[attribute[0]])

                    result.append(data)

            if not clean is None:
                cursor.close()
                cursor = cnx.cursor()
                cursor.execute(clean)

            return result
        except:
            print traceback.format_exc()
            curframe = inspect.currentframe()
            calframe = inspect.getouterframes(curframe, 2)
            send_to_debug_console(sql, "error", calframe[1][3])
        finally:
            try:
                cnx.commit()
                cursor.close()
                cnx.close()
            except:
                pass

    @classmethod
    def fetch_one(cls, sql, params=(), tables=[], database=None, dbtype='master', as_dict=False):
        attributes = inspect.getmembers(cls, lambda a: not (inspect.isroutine(a)))
        attributes = [a for a in attributes if not (a[0].startswith('__') and a[0].endswith('__'))]

        if isinstance(sql, int):
            id = sql
            sql = "SELECT "

            separator = ""
            for attribute in attributes:
                sql = "{0} {1}{2}".format(sql, separator, attribute[0])
                separator = ","

            sql = "{0} FROM #table# WHERE id = {1}".format(sql, id)

        if sql.startswith("WHERE") or sql.startswith("where"):
            where = sql
            sql = "SELECT "

            separator = ""
            for attribute in attributes:
                sql = "{0} {1}{2}".format(sql, separator, attribute[0])
                separator = ","

            sql = "{0} FROM #table# {1}".format(sql, where)

        if database is None:
            database = DEFAULT_DATABASE

        if cls.use_auth():
            database = "auth"

        try:

            sql = cls.fill_tables(sql, tables)

            cnx = Database(database, dbtype)

            cursor = cnx.cursor()

            cursor.execute(sql, params)

            curframe = inspect.currentframe()
            calframe = inspect.getouterframes(curframe, 2)
            send_to_debug_console(cursor._executed, "success", calframe[1][3])

            result = None

            columns = tuple([d[0].decode('utf8') for d in cursor.description])

            row = cursor.fetchone()

            if row == None:

                if as_dict:
                    return {}

                return None

            row = dict(zip(columns, row))

            if as_dict:
                return row

            data = cls()

            for attribute in attributes:
                if row.has_key(attribute[0]):
                    setattr(data, attribute[0], row[attribute[0]])

            result = data

            return result
        except:
            print traceback.format_exc()
            curframe = inspect.currentframe()
            calframe = inspect.getouterframes(curframe, 2)
            send_to_debug_console(sql, "error", calframe[1][3])
        finally:
            try:
                cnx.commit()
                cursor.close()
                cnx.close()
            except:
                pass

    def create(self):
        attributes = inspect.getmembers(self, lambda a: not (inspect.isroutine(a)))
        attributes = [a for a in attributes if not (a[0].startswith('__') and a[0].endswith('__'))]

        sql = "INSERT INTO #table# ("

        separator = ""
        for attribute in attributes:
            sql = "{0}{1}{2}".format(sql, separator, attribute[0])
            separator = ","

        sql = "{0}) VALUES (".format(sql)
        separator = ""
        params = []
        for attribute in attributes:
            sql = "{0}{1}%s".format(sql, separator)
            params.append(attribute[1])
            separator = ","

        sql = "{0})".format(sql)

        return self.execute(sql, tuple(params))

    @classmethod
    def get_table_name(cls):
        return ""

    @classmethod
    def use_auth(cls):
        return False


if __name__ == '__main__':

    class Config:
        database_user = 'root'
        database_password = ''
        database = 'iab'
        database_host = '127.0.0.1'

    class Urls(Model):
        id = 0
        url = None

        @classmethod
        def get_table_name(cls):
            return "urls"

    print "Sin parametros"

    urls = Urls.fetch_all("SELECT u.id, u.url FROM #table# as u")

    for url in urls:
        print "id: {0}, url: {1}".format(url.id, url.url)

    print "Con parametros"

    urls = Urls.fetch_all("SELECT u.id, u.url FROM #table# as u WHERE u.id = %s", (1,))

    for url in urls:
        print "id: {0}, url: {1}".format(url.id, url.url)
